## 회사 전원 장치의 모니터링을 위한 S/W

### 설치 방법

* python 최신 버전 설치
* pip install -r package.txt
* 혹시 에러가 발생하는 경우 namju.yoon@pstek.co.kr 로 문의


### 실행 방법
* python escmonitorying.py : 정전척 전원장치 - 반도체 웨이퍼 제어용
* 해당 프로그램은 전원 장치와 연결이 필요 없고, "단말기 정보요청"을 누르면 시뮬에이션한 데이터를 불러오고 그래프를 그려줌
* 본래 50ms 마다 데이터를 읽어오고, 정밀한 데이터 제어를 위해 500ms 마다 그래프를 그림
---
* python epcmonitorying.py : 집진기 전원장치 - 미세먼지를 걸어주기 위함 (발전소, 반도체/디스플레이와 같은 클린룸 구성용)
* 해당 프로그램은 정밀 제어가 아니고 전원 장치 내에 코일의 배수를 통해서 전압을 단계적으로 높여주는 것을 모니터링함
* 데이터 읽어오는 주기를 선택할 수 있음
---
[https://gitlab.com/namju.yoon/shareformonitoring/-/blob/master/images/epc1.png](https://gitlab.com/namju.yoon/shareformonitoring/-/blob/master/images/epc1.png)
