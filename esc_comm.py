import sys
import serial
import time 

from PyQt5.QtWidgets import QWidget, QLabel, QLineEdit, QPushButton, QDialogButtonBox
from PyQt5.QtWidgets import QGroupBox, QBoxLayout, QVBoxLayout, QHBoxLayout, QGridLayout
from PyQt5.QtWidgets import QApplication, QDialog, QRadioButton, QSpinBox, QDoubleSpinBox
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtGui import QPixmap, QFont

import esc_serial as device
from ctypes import c_uint16

class ESCComm(QDialog):
    def __init__(self, Dialog, client, initial):
        super().__init__()
        self.client = client
        self.initial = initial
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        self.setGeometry(100, 100, 400, 410)
        font = QFont()
        font.setPointSize(12)
    # First Region
        self.voltage1 = QSpinBox()
        self.voltage1.setRange(-2500, 2500)
        self.voltage1.setSingleStep(10)
        self.voltage1_changed = False

        self.current1 = QDoubleSpinBox()
        self.current1.setRange(0, 10.00)
        self.current1.setSingleStep(0.1)
        self.current1_changed = False

        self.voltage2 = QSpinBox()
        self.voltage2.setRange(-2500, 2500)
        self.voltage2.setSingleStep(10)
        self.voltage2_changed = False

        self.current2 = QDoubleSpinBox()
        self.current2.setRange(0, 10.00)
        self.current2.setSingleStep(0.1)
        self.current2_changed = False

        self.leak_fault_level = QDoubleSpinBox()
        self.leak_fault_level.setRange(0, 1.00)
        self.leak_fault_level.setSingleStep(0.1)
        self.leak_fault_level_changed = False

        self.ro_min_fault = QDoubleSpinBox()
        self.ro_min_fault.setRange(1.0, 999.9)
        self.ro_min_fault_changed = False

        self.up_time = QDoubleSpinBox()
        self.up_time.setRange(0.30, 9.90)
        self.up_time.setValue(0.30)
        self.up_time_changed = False

        self.down_time = QDoubleSpinBox()
        self.down_time.setRange(0.3, 9.90)
        self.down_time.setSingleStep(0.1)
        self.down_time_changed = False
        
        self.rd_mode_off = QRadioButton("OFF")
        self.rd_mode_off.setChecked(True)
        self.rd_mode_off.clicked.connect(lambda:self.radioValueClicked(self.rd_mode_off, "MODE"))
        self.rd_mode_on = QRadioButton("ON")
        self.rd_mode_on.clicked.connect(lambda:self.radioValueClicked(self.rd_mode_on, "MODE"))
        self.group_mode = QGroupBox("")
        mode_layout = QHBoxLayout()
        self.group_mode.setLayout(mode_layout)
        mode_layout.addWidget(self.rd_mode_off)
        mode_layout.addWidget(self.rd_mode_on)
        self.mode_changed = False
        self.mode_value = 0

    # Second Region
        self.toggle_count = QSpinBox()
        self.toggle_count.setRange(1, 99)
        self.toggle_count_changed = False

        self.slope = QSpinBox()
        self.slope.setRange(10, 100)
        self.slope_changed = False

        self.coeff = QSpinBox()
        self.coeff.setRange(1, 10)
        self.coeff_changed = False

        self.rd_select_internal = QRadioButton("INTERNAL")
        self.rd_select_internal.setChecked(True)
        self.rd_select_internal.clicked.connect(lambda:self.radioValueClicked(self.rd_select_internal, "SELECT"))
        self.rd_select_remote = QRadioButton("REMOTE")
        self.rd_select_remote.clicked.connect(lambda:self.radioValueClicked(self.rd_select_remote, "SELECT"))
        self.group_select = QGroupBox("")
        select_layout = QHBoxLayout()
        self.group_select.setLayout(select_layout)
        select_layout.addWidget(self.rd_select_internal)
        select_layout.addWidget(self.rd_select_remote)
        self.select_changed = False
        self.select_value = 0

        self.local_address = QSpinBox()
        self.local_address.setRange(1, 31)
        self.local_address_changed = False

        self.arc_delay = QDoubleSpinBox()
        self.arc_delay.setRange(10.00, 50.00)
        self.arc_delay_changed = False

        self.arc_rate = QSpinBox()
        self.arc_rate.setRange(1, 1000)
        self.arc_rate.setSingleStep(5)
        self.arc_rate_changed = False

    # Third Region
        self.rd_toggle_off = QRadioButton("FORWARD")
        self.rd_toggle_off.setChecked(True)
        self.rd_toggle_off.clicked.connect(lambda:self.radioValueClicked(self.rd_toggle_off, "TOGGLE"))
        self.rd_toggle_on = QRadioButton("REVERSE")
        self.rd_toggle_on.clicked.connect(lambda:self.radioValueClicked(self.rd_toggle_on, "TOGGLE"))
        self.group_toggle = QGroupBox("")
        toggle_layout = QHBoxLayout()
        self.group_toggle.setLayout(toggle_layout)
        toggle_layout.addWidget(self.rd_toggle_off)
        toggle_layout.addWidget(self.rd_toggle_on)
        self.toggle_changed = False
        self.toggle_value = 0

        self.rd_arc_off = QRadioButton("OFF")
        self.rd_arc_off.setChecked(True)
        self.rd_arc_off.clicked.connect(lambda:self.radioValueClicked(self.rd_arc_off, "ARC"))
        self.rd_arc_on = QRadioButton("ON")
        self.rd_arc_on.clicked.connect(lambda:self.radioValueClicked(self.rd_arc_on, "ARC"))
        self.group_arc = QGroupBox("")
        arc_layout = QHBoxLayout()
        self.group_arc.setLayout(arc_layout)
        arc_layout.addWidget(self.rd_arc_off)
        arc_layout.addWidget(self.rd_arc_on)
        self.arc_changed = False
        self.arc_value = 0

        self.rd_ocp_off = QRadioButton("OFF")
        self.rd_ocp_off.setChecked(True)
        self.rd_ocp_off.clicked.connect(lambda:self.radioValueClicked(self.rd_ocp_off, "OCP"))
        self.rd_ocp_on = QRadioButton("ON")
        self.rd_ocp_on.clicked.connect(lambda:self.radioValueClicked(self.rd_ocp_on, "OCP"))
        self.group_ocp = QGroupBox("")
        ocp_layout = QHBoxLayout()
        self.group_ocp.setLayout(ocp_layout)
        ocp_layout.addWidget(self.rd_ocp_off)
        ocp_layout.addWidget(self.rd_ocp_on)
        self.ocp_changed = False
        self.ocp_value = 0

        self.target_cap = QSpinBox()
        self.target_cap.setRange(1, 15000)
        self.target_cap.setSingleStep(100)
        self.target_cap_changed = False

        self.cap_deviation = QSpinBox()
        self.cap_deviation.setRange(0, 100)
        self.cap_deviation_changed = False

        self.time_delay = QSpinBox()
        self.time_delay.setRange(0, 100)
        self.time_delay.setValue(10)

        self.display_value()
    
        self.voltage1.valueChanged.connect(lambda:self.spinValueChanged(self.voltage1, 'VOLTAGE1'))
        self.current1.valueChanged.connect(lambda:self.spinValueChanged(self.current1, 'CURRENT1'))
        self.voltage2.valueChanged.connect(lambda:self.spinValueChanged(self.voltage2, 'VOLTAGE2'))
        self.current2.valueChanged.connect(lambda:self.spinValueChanged(self.current2, 'CURRENT2'))
        self.leak_fault_level.valueChanged.connect(lambda:self.spinValueChanged(self.leak_fault_level, 'LEAKFAULT'))
        self.ro_min_fault.valueChanged.connect(lambda:self.spinValueChanged(self.ro_min_fault, 'ROMIN'))
        self.up_time.valueChanged.connect(lambda:self.spinValueChanged(self.up_time, 'UPTIME'))
        self.down_time.valueChanged.connect(lambda:self.spinValueChanged(self.down_time, 'DOWNTIME'))

        self.toggle_count.valueChanged.connect(lambda:self.spinValueChanged(self.toggle_count, 'TOGGLECOUNT'))
        self.slope.valueChanged.connect(lambda:self.spinValueChanged(self.slope, 'SLOPE'))
        self.coeff.valueChanged.connect(lambda:self.spinValueChanged(self.coeff, 'COEFF'))
        self.local_address.valueChanged.connect(lambda:self.spinValueChanged(self.local_address, 'LOCALADD'))
        self.arc_delay.valueChanged.connect(lambda:self.spinValueChanged(self.arc_delay, 'ARCDELAY'))
        self.arc_rate.valueChanged.connect(lambda:self.spinValueChanged(self.arc_rate, 'ARCRATE'))
        self.target_cap.valueChanged.connect(lambda:self.spinValueChanged(self.target_cap, 'TAGETCAP'))
        self.cap_deviation.valueChanged.connect(lambda:self.spinValueChanged(self.cap_deviation, 'CAPDEVI'))
    
        # Main 
        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

        # 통신 포트  설정(Serial Port)"
        group_setting = QGroupBox("Setting Parameters")
        grid_box = QGridLayout()

        grid_box.addWidget(QLabel("SET OUT VOLTAGE1:"), 0, 0)
        grid_box.addWidget(self.voltage1, 0, 1, 1, 2)
        grid_box.addWidget(QLabel("-2500 ~ 2500"), 0, 3)
        grid_box.addWidget(QLabel("V"), 0, 4)

        grid_box.addWidget(QLabel("SET OUT CURRENT1: "), 1, 0)
        grid_box.addWidget(self.current1, 1, 1, 1, 2)
        grid_box.addWidget(QLabel("0 ~ 10.00"), 1, 3)
        grid_box.addWidget(QLabel("mA"), 1, 4)

        grid_box.addWidget(QLabel("DSET OUT VOLTAGE2:"), 2, 0)
        grid_box.addWidget(self.voltage2, 2, 1, 1, 2)
        grid_box.addWidget(QLabel("-2500 ~ 2500"), 2, 3)
        grid_box.addWidget(QLabel("V"), 2, 4)

        grid_box.addWidget(QLabel("SET OUT CURRENT2:"), 3, 0)
        grid_box.addWidget(self.current2, 3, 1, 1, 2)
        grid_box.addWidget(QLabel("0 ~ 10.00"), 3, 3)
        grid_box.addWidget(QLabel("mA"), 3, 4)

        grid_box.addWidget(QLabel("SET LEAK FAULT LEVEL: "), 4, 0)
        grid_box.addWidget(self.leak_fault_level, 4, 1, 1, 2)
        grid_box.addWidget(QLabel("0 ~ 1.00"), 4, 3)
        grid_box.addWidget(QLabel("mA"), 4, 4)

        grid_box.addWidget(QLabel("SET RO MIN FALUT"), 5, 0)
        grid_box.addWidget(self.ro_min_fault, 5, 1, 1, 2)
        grid_box.addWidget(QLabel("0 ~ 999"), 5, 3)
        grid_box.addWidget(QLabel("Kohm"), 5, 4)

        grid_box.addWidget(QLabel("RAMP UP TIME"), 6, 0)
        grid_box.addWidget(self.up_time, 6, 1, 1, 2)
        grid_box.addWidget(QLabel("0.3 ~ 9.9 "), 6, 3)
        grid_box.addWidget(QLabel("sec"), 6, 4)

        grid_box.addWidget(QLabel("RAMP DOWN TIME"), 7, 0)
        grid_box.addWidget(self.down_time, 7, 1, 1, 2)
        grid_box.addWidget(QLabel("0.3 ~ 9.9"), 7, 3)
        grid_box.addWidget(QLabel("sec"), 7, 4)

        grid_box.addWidget(QLabel("AUTO TOGGLE MODE:"), 8, 0)
        grid_box.addWidget(self.group_mode, 8, 1, 1, 2)
        grid_box.addWidget(QLabel("(O:OFF, 1:ON)"), 8, 3)

        grid_box.addWidget(QLabel("AUTO TOGGLE COUNT"), 9, 0)
        grid_box.addWidget(self.toggle_count, 9, 1, 1, 2)
        grid_box.addWidget(QLabel("1 ~ 99"), 9, 3)

        grid_box.addWidget(QLabel("SET SLOPE"), 10, 0)
        grid_box.addWidget(self.slope, 10, 1, 1, 2)
        grid_box.addWidget(QLabel("10 ~ 100"), 10, 3)

        grid_box.addWidget(QLabel("SET COEFF"), 11, 0)
        grid_box.addWidget(self.coeff, 11, 1, 1, 2)
        grid_box.addWidget(QLabel("1 ~ 10"), 11, 3)

        grid_box.addWidget(QLabel("ON/OFF SELECTION"), 12, 0)
        grid_box.addWidget(self.group_select, 12, 1, 1, 2)
        grid_box.addWidget(QLabel("(O:INTERNAL, 1:REMOTE)"), 12, 3)

        grid_box.addWidget(QLabel("LOCAL ADDRESS"), 13, 0)
        grid_box.addWidget(self.local_address, 13, 1, 1, 2)
        grid_box.addWidget(QLabel("1 ~ 31"), 13, 3)

        grid_box.addWidget(QLabel("ARC DELAY"), 14, 0)
        grid_box.addWidget(self.arc_delay, 14, 1, 1, 2)
        grid_box.addWidget(QLabel("10.00 ~ 50.00 "), 14, 3)
        grid_box.addWidget(QLabel("m sec"), 14, 4)

        grid_box.addWidget(QLabel("ARC RATE"), 15, 0)
        grid_box.addWidget(self.arc_rate, 15, 1, 1, 2)
        grid_box.addWidget(QLabel("1 ~ 1000 "), 15, 3)
        grid_box.addWidget(QLabel("a/s"), 15, 4)

        grid_box.addWidget(QLabel("TOGGLE :"), 16, 0)
        grid_box.addWidget(self.group_toggle, 16, 1, 1, 2)
        grid_box.addWidget(QLabel("(0:FORWARD, 1:REVERSE)"), 16, 3)

        grid_box.addWidget(QLabel("ARC CONTROL :(O:OFF, 1:ON)"), 17, 0)
        grid_box.addWidget(self.group_arc, 17, 1, 1, 2)
        grid_box.addWidget(QLabel("(O:OFF, 1:ON)"), 17, 3)

        grid_box.addWidget(QLabel("OCP CONTROL:"), 18, 0)
        grid_box.addWidget(self.group_ocp, 18, 1, 1, 2)
        grid_box.addWidget(QLabel("(O:OFF, 1:ON)"), 18, 3)

        grid_box.addWidget(QLabel("TARGET CAPACITOR"), 19, 0)
        grid_box.addWidget(self.target_cap, 19, 1, 1, 2)
        grid_box.addWidget(QLabel("1 ~ 15000 "), 19, 3)
        grid_box.addWidget(QLabel("pF"), 19, 4)

        grid_box.addWidget(QLabel("CAP DEVIATION"), 20, 0)
        grid_box.addWidget(self.cap_deviation, 20, 1, 1, 2)
        grid_box.addWidget(QLabel("0 ~ 100 "), 20, 3)
        grid_box.addWidget(QLabel("%"), 20, 4)

        grid_box.addWidget(QLabel("TIME DELAY"), 21, 0)
        grid_box.addWidget(self.time_delay, 21, 1, 1, 2)
        grid_box.addWidget(QLabel("0 ~ 100"), 21, 3)
        grid_box.addWidget(QLabel("Sec"), 21, 4)

        group_setting.setLayout(grid_box)
        main_layer.addWidget(group_setting)

        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.addButton('MODIFY PARAMS', QDialogButtonBox.AcceptRole)
        self.buttonBox.addButton('CANCEL', QDialogButtonBox.RejectRole)
        self.buttonBox.addButton('DATA CLEAR', QDialogButtonBox.ResetRole)
        main_layer.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)

    def display_value(self):
        if self.initial:
            self.voltage1.setValue(self.initial["voltage1"])
            self.current1.setValue(self.initial["current1"])
            self.voltage2.setValue(self.initial["voltage2"])
            self.current2.setValue(self.initial["current1"])
            self.leak_fault_level.setValue(self.initial["leak_fault_level"])
            self.ro_min_fault.setValue(self.initial["ro_min_fault"])
            self.up_time.setSingleStep(self.initial["up_time"])
            self.down_time.setValue(self.initial["down_time"])
            self.toggle_count.setValue(self.initial["toggle_count"])
            self.slope.setValue(self.initial["slope"])
            self.coeff.setValue(self.initial["coeff"])
            self.local_address.setValue(self.initial["local_address"])
            self.arc_delay.setValue(self.initial["arc_delay"])
            self.arc_rate.setValue(self.initial["arc_rate"])
            self.target_cap.setValue(self.initial["target_cap"])
            self.cap_deviation.setValue(self.initial["cap_deviation"])
        else:
            print("No data !!")

    def on_accepted(self):
        if self.voltage1_changed:
            value = int(self.voltage1.value())
            if value <= 0:
                value = value & 0xffff
            device.write_registers(self.client, device.WRITE_ADDRESS_VOLTAGE1, value)
            self.voltage1_changed = False

        if self.current1_changed:
            value = float(self.current1.value()) * 1000
            res = device.write_registers(self.client, device.WRITE_ADDRESS_CURRENT1, value)
            self.current1_changed = False

        if self.voltage2_changed:
            value = int(self.voltage2.value())
            if value <= 0:
                value = value & 0xffff
            res = device.write_registers(self.client, device.WRITE_ADDRESS_VOLTAGE2, value)
            self.voltage2_changed = False

        if self.current2_changed:
            value = float(self.current2.value()) * 1000
            res = device.write_registers(self.client, device.WRITE_ADDRESS_CURRENT2, value)
            self.current2_changed  = False

        if self.leak_fault_level_changed:
            value = float(self.leak_fault_level.value()) * 1000
            res = device.write_registers(self.client, device.WRITE_LEAK_LEVEL, value)
            self.leak_fault_level_changed = False

        if self.ro_min_fault_changed:
            value = float(self.ro_min_fault.value()) * 1000
            res = device.write_registers(self.client, device.WRITE_RO_MIN_FAULT, value)
            self.ro_min_fault_changed = False

        if self.up_time_changed:
            value = float(self.up_time.value()) * 10
            value = c_uint16(value).value
            res = device.write_registers(self.client, device.WRITE_RAMP_UP_TIME, value)
            self.up_time_changed = False

        if self.down_time_changed:
            value = float(self.down_time.value()) * 10
            res = device.write_registers(self.client, device.WRITE_RAMP_DOWN_TIME, value)
            self.down_time_changed = False

        if self.mode_changed:
            value = self.mode_value
            res = device.write_registers(self.client, device.WRITE_TOGGLE_MODE, value)
            self.mode_changed = False

        if self.toggle_count_changed:
            value = int(self.toggle_count.value())
            res = device.write_registers(self.client, device.WRITE_TOGGLE_COUNT, value)
            self.toggle_count_changed = False

        if self.slope_changed:
            value = int(self.slope.value())
            res = device.write_registers(self.client, device.WRITE_SET_SLOPE, value)
            self.slope_changed = False
        
        if self.coeff_changed:
            value = int(self.coeff.value())
            res = device.write_registers(self.client, device.WRITE_SET_COEFF, value)
            self.slope_changed = False

        if self.select_changed:
            value = self.select_value
            res = device.write_registers(self.client, device.WRITE_TOGGLE_MODE, value)
            self.select_changed = False

        if self.local_address_changed:
            value = int(self.local_address.value())
            res = device.write_registers(self.client, device.WRITE_LOCAL_ADDRESS, value)
            self.slope_changed = False

        if self.arc_delay_changed:
            value = float(self.arc_delay.value()) * 100
            res = device.write_registers(self.client, device.WRITE_ARC_DELAY, value)
            self.slope_changed = False

        if self.arc_rate_changed:
            value = int(self.arc_rate.value())
            res = device.write_registers(self.client, device.WRITE_ARC_RATE, value)
            self.slope_changed = False

        if self.toggle_changed:
            value = self.toggle_value
            res = device.write_registers(self.client, device.WRITE_TOGGLE, value)
            self.toggle_changed = False

        if self.arc_changed:
            value = self.arc_value
            res = device.write_registers(self.client, device.WRITE_ARC_CONTROL, value)
            self.arc_changed = False

        if self.ocp_changed:
            value = self.ocp_value
            res = device.write_registers(self.client, device.WRITE_OCP_CONTROL, value)
            self.ocp_changed = False

        if self.target_cap_changed:
            value = int(self.target_cap.value())
            res = device.write_registers(self.client, device.WRITE_TARGET_CAP, value)
            self.target_cap_changed = False

        if self.cap_deviation_changed:
            value = int(self.cap_deviation.value())
            res = device.write_registers(self.client, device.WRITE_CAP_DEVIATION, value)
            self.cap_deviation_changed = False
        
        time.sleep(2)
        self.initial = device.read_setting_value(self.client)
        self.display_value()
        self.accept()


    def radioValueClicked(self, btn, mode):
        if mode == 'MODE':
            self.mode_changed = True
            if btn.text() == 'OFF':
                self.mode_value = 0
            else:
                self.mode_value = 1
        elif mode == 'SELECT':
            self.select_changed = True
            if btn.text() == 'OFF':
                self.select_value = 0
            else:
                self.select_value = 1
        elif mode == 'TOGGLE':
            self.toggle_changed = True
            if btn.text() == 'FORWARD':
                self.toggle_value = 0
            else:
                self.toggle_value = 1
        elif mode == 'ARC':
            self.arc_changed = True
            if btn.text() == 'OFF':
                self.arc_value = 0
            else:
                self.arc_value = 1
        elif mode == 'OCP':
            self.ocp_changed = True
            if btn.text() == 'OFF':
                self.ocp_value = 0
            else:
                self.ocp_value = 1

    def spinValueChanged(self, edit, mode):
        if mode == 'VOLTAGE1':
            self.voltage1_changed = True
        elif mode == 'CURRENT1':
            self.current1_changed = True
        elif mode == 'VOLTAGE2':
            self.voltage2_changed = True
        elif mode == 'CURRENT2':
            self.current2_changed = True
        elif mode == 'LEAKFAULT':
            self.leak_fault_level_changed = True
        elif mode == 'ROMIN':
            self.ro_min_fault_changed = True
        elif mode == 'UPTIME':
            self.up_time_changed = True
        elif mode == 'DOWNTIME':
            self.down_time_changed = True
        elif mode == 'TOGGLECOUNT':
            self.toggle_count_changed = True
        elif mode == 'SLOPE':
            self.slope_changed = True
        elif mode == 'COEFF':
            self.coeff_changed = True
        elif mode == 'LOCALADD':
            self.local_address_changed = True
        elif mode == 'ARCDELAY':
            self.arc_delay_changed = True
        elif mode == 'ARCRATE':
            self.arc_rate_changed = True
        elif mode == 'TAGETCAP':
            self.target_cap_changed = True
        elif mode == 'CAPDEVI':
            self.cap_deviation_changed = True


if __name__ == "__main__":
    app = QApplication(sys.argv)
    Dialog = QDialog()
    client = None
    mywindow = ESCComm(Dialog, client)
    mywindow.show()
    app.exec_()