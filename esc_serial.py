from sys import platform
from random import randint
import serial
import time
from ctypes import c_int16

## RTU
PTYPE = 'rtu'
# PTYPE = 'tcp'

if PTYPE == 'tcp':
    PORT = 1502
    PTYPE = 'tcp'

# READ_ADDRESS_DEVICE_STATUS = 0x00 
# READ_ADDRESS_DEVICE_WARNING = 0x01
# READ_ADDRESS_DEVICE_FAIL = 0x02
READ_ADDRESS_FAULT = 0x03

READ_ADDRESS_VOLTAGE1 = 0x05 # range -2500 ~ +2500 V
READ_ADDRESS_CURRENT1 = 0x06  # range 0 ~ 10000 mA
READ_ADDRESS_VOLTAGE2 = 0x07 # range -2500 ~ +2500 V
READ_ADDRESS_CURRENT2 = 0x08 # range 0 ~ 10000 mA

READ_ADDRESS_VBIA = 0x09 # range 0 ~ 1250
READ_ADDRESS_VCS = 0x0A # range 0.000 ~ 3.300 V

READ_ADDRESS_ICS = 0x0B # range 0.000 ~ 3.300 V
READ_ADDRESS_CP = 0x0C # range 0.000 ~ 3.300 V
READ_ADDRESS_DECHUCK_START = 0x0D # range 0.000 ~ 3.300 V
READ_ADDRESS_DECHUCK_END = 0x0E # range 0.000 ~ 3.300 V

# WRITE 후 READ DATA
READ_WRITE_VOLTAGE1 = 0x10 # range -2500 ~ +2500 V
READ_WRITE_CURRENT1 = 0x11  # range 0 ~ 10000 mA
READ_WRITE_VOLTAGE2 = 0x12 # range -2500 ~ +2500 V
READ_WRITE_CURRENT2 = 0x13 # range 0 ~ 10000 mA

READ_WRITE_LEAK_FAULT_LEVEL = 0x14 
READ_WRITE_ROMIN = 0x15 
READ_WRITE_UPTIME = 0x16 
READ_WRITE_DOWNTIME = 0x17 

# READ_WRITE = 0x18 


# WRITE_RUN_ADDRESS = 0x00
RUN_VALUE = 2
STOP_VALUE = 0

WRITE_BIT_POWER_ON = 0x00
WRITE_BIT_FAULT_CLEAR = 0x02

WRITE_ADDRESS_VOLTAGE1 = 0x01 # range -2500 ~ +2500 V
WRITE_ADDRESS_CURRENT1 = 0x02  # range 0 ~ 10000 mA

WRITE_ADDRESS_VOLTAGE2 = 0x03 # range -2500 ~ +2500 V
WRITE_ADDRESS_CURRENT2 = 0x04 # range 0 ~ 10000 mA

WRITE_LEAK_LEVEL = 0x05 
WRITE_RO_MIN_FAULT = 0x06 
WRITE_RAMP_UP_TIME = 0x07 
WRITE_RAMP_DOWN_TIME = 0x08 
WRITE_TOGGLE_MODE = 0x09 
WRITE_TOGGLE_COUNT = 0x0A 

WRITE_SET_SLOPE = 0x0B 
WRITE_SET_COEFF = 0x0C 
WRITE_ONOFF_SELECT = 0x0E 
WRITE_LOCAL_ADDRESS = 0x0F 
WRITE_ARC_DELAY = 0x10 
WRITE_ARC_RATE = 0x11 
WRITE_TOGGLE = 0x12 
WRITE_ARC_CONTROL = 0x13 
WRITE_OCP_CONTROL = 0x14 
WRITE_TARGET_CAP = 0x15 
WRITE_CAP_DEVIATION = 0x16 

UNIT = 0x1
END_VALUE = 0x00ed

FORWARD = 0 
REVERSE = 1 

from pymodbus.constants import Defaults
Defaults.RetryOnEmpty = True
Defaults.Timeout = 5
Defaults.Retries = 3

def make_connect(port, ptype='rtu', speed=38400, bytesize=8, parity='N', stopbits=1):
    print(port, ptype, speed, bytesize, parity, stopbits)
    if ptype == 'rtu':
        from pymodbus.client.sync import ModbusSerialClient as ModbusClient
        client = ModbusClient(method=ptype, port=port, timeout=1,
                          baudrate=speed, bytesize=bytesize, parity=parity, stopbits=stopbits)
    else: 
        from pymodbus.client.sync import ModbusTcpClient as ModbusClient
        client = ModbusClient('localhost', port=PORT)
    client.connect()

    if client:
        print("{}".format('*'*40))
        print("******************* DUN SUCCESS *********************", client)
        print("{}".format('*'*40))

    return client


def make_qrport(port):
    client = serial.Serial(port)
    return client


# Device Main Data 읽어오기 
def read_registers(client, address, count):
    try:
        result = client.read_input_registers(address, 10, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : ", )
        response = result.registers
    except Exception as e:
        response = []
        print("Error read_input_registers : ", e)
    return response


# TEST 용 SIMU
from random import randint
def read_registers_simu(client, address, count):
    data = []
    data.append(1365 + randint(-200, 200))    #result[0]
    data.append(2726 + randint(-200, 200))    #result[1]
    data.append(4087 + randint(-200, 200))    #result[2]
    data.append(5448 + randint(-200, 200))    #result[3]
    data.append(6810 + randint(-200, 200))    #result[4]
    data.append(8171 + randint(-200, 200))    #result[5]
    data.append(9532 + randint(-200, 200))    #result[6]
    data.append(1093 + randint(-50, 50))     #result[7]
    data.append(5893 + randint(-200, 200))   #result[8]
    data.append(1300 + randint(-30, 30))    #result[9]

    return data

def read_fault(client):
    # return random.randint(24200, 2500)
    try:
        result = client.read_input_registers(READ_ADDRESS_FAULT, 1, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : ", )
        response = result.registers
        result = c_int16(response[0]).value
    
    except Exception as e:
        print("read_fault ", e)
        result = 0
    return result


# Device Initial Setting Value 읽어오기 
def read_setting_value(client):
    data = {}
    try:
        response = client.read_input_registers(READ_WRITE_VOLTAGE1, 22, unit=UNIT)
        if response.function_code >= 0x80:
            print("Error Happened : ", )
            return data

        result = response.registers
        voltage1 = c_int16(result[0]).value
        voltage2 = c_int16(result[2]).value

        data = {}
        data['voltage1'] = voltage1
        data['current1'] = result[1] / 1000
        data['voltage2'] = voltage2
        data['current2'] = result[3] / 1000
        data['leak_fault_level'] = result[4] / 1000
        data['ro_min_fault'] = result[5] / 1000
        data['up_time'] = result[6] / 10
        data['down_time'] = result[7] / 10
        data['rd_mode'] = result[8]
        data['toggle_count'] = result[9]
        data['slope'] = result[10]
        data['coeff'] = result[11]
        data['rd_select'] = result[13]
        data['local_address'] = result[14]
        data['arc_delay'] = result[15] / 100
        data['arc_rate'] = result[16]
        data['rd_toggle'] = result[17]
        data['rd_arc'] = result[18]
        data['rd_ocp'] = result[19]
        data['target_cap'] = result[20]
        data['cap_deviation'] = result[21]
        return data

    except Exception as e:
        response = []
        print("Error read_setting_value : ", e)
    return response

# Write Register
def write_registers(client, start_address, value):
    print(start_address, value)
    try:
        response = client.write_registers(start_address, value, unit=UNIT)
        print(start_address, value)
    except Exception as e:
        response = []
        print("Error write_registers : ", e)
    return response


# Read FeedBack Data
def read_feedback(client, address):
    try:
        result = client.read_input_registers(address, 1, unit=UNIT)
        if result.function_code >= 0x80:
            print("Error Happened : ", )
        response = result.registers
    except Exception as e:
        response = []
        print("Error read_input_registers : ", e)
    return response

def device_run(client):
    response = write_registers(client, WRITE_BIT_POWER_ON, RUN_VALUE)
    return response


def device_stop(client):
    response = write_registers(client, WRITE_BIT_POWER_ON, STOP_VALUE)
    return response
