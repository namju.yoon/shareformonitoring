#!/usr/bin/env python
# coding: utf-8

# 예제 내용
# * 기본 위젯을 사용하여 기본 창을 생성
# * 다양한 레이아웃 위젯 사용
import os
import sys
import numpy as np
import pandas as pd
import serial
import time
import json, codecs

from random import randint
from PyQt5.QtWidgets import QWidget, QLabel, QTextEdit, QPushButton, QDesktopWidget, QMainWindow
from PyQt5.QtWidgets import QGroupBox, QBoxLayout, QVBoxLayout, QHBoxLayout, QGridLayout
from PyQt5.QtWidgets import QApplication, QDialog, QStatusBar, QFileDialog, QCheckBox
from PyQt5 import QtCore
from PyQt5.QtCore import QDate, Qt
from PyQt5.QtGui import QPixmap
from itertools import count
from setup_device import SettingWin
from zoom import GraphWindow, ZoomWindow, ZoomWindow6
from save_data import DataWin
import pyqtgraph as pyGraph
import pyqtgraph.exporters
import esc_serial as device
from esc_comm import ESCComm

from ctypes import c_int16
from threading import Timer

# import logging
# FORMAT = ('%(asctime)-15s %(threadName)-15s '
#           '%(levelname)-8s %(module)-15s:%(lineno)-8s %(message)s')
# logging.basicConfig(format=FORMAT)
# log = logging.getLogger()
# log.setLevel(logging.DEBUG)

def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.abspath("."), relative_path)


# repeat function control timer of threading

class MainWindow(QMainWindow):
    def __init__(self):
        super(MainWindow, self).__init__()

        self.toggle = device.FORWARD

        self.faultmessage = {
            0 : "Fault01-0 : FAULT LEAK CURRENT OVER  ",
            1 : "Fault01-1 : FAULT HW OUT CURRENT OVER1  ",
            2 : "Fault01-2 : FAULT HW OUT CURRENT OVER2  ",
            3 : "Fault01-3 : FAULT HW OUT VOLTAGE OVER1  ",
            4 : "Fault01-4 : FAULT HW OUT VOLTAGE OVER2  ",
            5 : "Fault01-5 : FAULT EXT INTERLOCK  ",
            6 : "Fault01-6 : FAULT OUT CURRENT OVER1  ",
            7 : "Fault01-7 : FAULT OUT CURRENT OVER2  ",
            8 : "Fault01-8 : FAULT OUT VOLTAGE OVER1  ",
            9 : "Fault01-9 : FAULT OUT VOLTAGE OVER2  ",
            10: "Fault01-10 : FAULT COMMUNICATION ERROR  ",
            11: "Fault01-11 : FAULT TOO MANY ARC  ",
            12: "Fault01-12 : FAULT RO MIN   ",
            13: "Fault01-13 : RESONANT CURRENT OVER1  ",
            14: "Fault01-14 : RESONANT CURRENT OVER2  ",
        }

        self.timer = QtCore.QTimer()
        self.stoptimer = QtCore.QTimer()

        self.faulttimer = QtCore.QTimer()

        self.com_setting_flag = False
        self.com_open_flag = False

        self.client = None
        self.gen_port = None
        self.com_speed = None
        self.com_data = None
        self.com_parity = None
        self.com_stop = None

        self.stopFlag = True
        self.graph_flag = False
        self.countN = 0

        self.initial = {}
        self.time_delay= 3

        self.view_voltage1 = True
        self.view_current1 = True
        self.view_voltage2 = True
        self.view_current2 = True
        self.view_vbia = True
        self.view_cps = True
        self.view_dechuckstart = True
        self.view_dechuckend = True

        self.indexs = np.array([])
        self.times = np.array([])
        self.voltage1s = np.array([])
        self.current1s = np.array([])
        self.voltage2s = np.array([])
        self.current2s = np.array([])
        self.vbias = np.array([])
        self.vcss = np.array([])
        self.icss = np.array([])
        self.cpss = np.array([])
        self.dechuckstarts = np.array([])
        self.dechuckends = np.array([])

        self.temp_indexs = np.array([])
        self.temp_times = np.array([])
        self.temp_voltage1s = np.array([])
        self.temp_current1s = np.array([])
        self.temp_voltage2s = np.array([])
        self.temp_current2s = np.array([])
        self.temp_vbias = np.array([])
        self.temp_vcss = np.array([])
        self.temp_icss = np.array([])
        self.temp_cpss = np.array([])
        self.temp_dechuckstarts = np.array([])
        self.temp_dechuckends = np.array([])

        self.pen_voltage1 = pyGraph.mkPen(width=1, color=(255, 0, 0))
        self.pen_current1 = pyGraph.mkPen(width=1, color=(0, 0, 255))
        self.pen_voltage2 = pyGraph.mkPen(width=1, color=(255, 191, 0))
        self.pen_current2 = pyGraph.mkPen(width=1, color=(0,0,128))
        self.pen_vbia = pyGraph.mkPen(width=1, color=(0, 255, 0))
        self.pen_cps = pyGraph.mkPen(width=1, color=(204, 153, 0))
        self.pen_dechuckstart = pyGraph.mkPen(width=1, color=(204, 0, 153))
        self.pen_dechuckend = pyGraph.mkPen(width=1, color=(102, 0, 204))

        self.initUI()

    def initUI(self):
        self.setWindowTitle("PSTEK ELECTROSTATIC CHUCK MONITORING")
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width() - 100, screensize.height()) 

        mainwidget = QWidget()                # 위젯의 인스턴스 생성만으로도 QMainWindow에 붙는다.
        self.setCentralWidget(mainwidget)

    ## Main Layout 설정
        main_layer = QVBoxLayout()
        mainwidget.setLayout(main_layer)

        bluefont = "color: #0000ff;"
        redfont = "color: #ff0000;"

        layout_setup = QHBoxLayout()
        ## replace #1st ###############
        # 전원 장치 연결
        grp_generator = QGroupBox("Connect Device(485/ModBus)")
        layout_generator = QHBoxLayout()
        self.btn_com_gen = QPushButton("SELECT PORT")
        self.label_gen_port = QLabel("N/A")
        self.btn_com_gen.clicked.connect(self.setting_generator)
        # self.btn_com_gen.setStyleSheet("padding: 5px; margin: 1px;")
        layout_generator.addWidget(self.btn_com_gen)
        layout_generator.addWidget(self.label_gen_port)
        grp_generator.setLayout(layout_generator)
        layout_setup.addWidget(grp_generator)


        # 단말기 RUN/STOP
        grp_runstop = QGroupBox("Run/Stop")
        layout_runstop = QHBoxLayout()
        self.btn_du_run = QPushButton("단말기 정보요청")
        # self.btn_du_run.setEnabled(False)
        self.btn_du_run.setStyleSheet(bluefont)
        self.btn_du_run.clicked.connect(self.run_device)
        
        self.btn_du_stop = QPushButton("정보 요청 Stop")
        self.btn_du_stop.setStyleSheet(redfont)
        self.btn_du_stop.clicked.connect(self.device_stop)
        # self.btn_du_stop.setEnabled(False)
        layout_runstop.addWidget(self.btn_du_run)
        layout_runstop.addWidget(self.btn_du_stop)
        grp_runstop.setLayout(layout_runstop)
        layout_setup.addWidget(grp_runstop)

        # 단말기 WRITE
        grp_communication = QGroupBox("Modify Parameters")
        layout_communication = QHBoxLayout()
        self.btn_communication = QPushButton("MODIFY PARAMS")
        self.btn_communication.setEnabled(False)
        self.btn_communication.clicked.connect(self.device_setting_modify)
        layout_communication.addWidget(self.btn_communication)
        grp_communication.setLayout(layout_communication)
        layout_setup.addWidget(grp_communication)

        # Toggle 기능 
        grp_toggle = QGroupBox("TOGGLE")
        layout_toggle = QHBoxLayout()
        self.btn_toggle = QPushButton("TOGGLE")
        self.btn_toggle.setEnabled(False)
        self.btn_toggle.clicked.connect(self.device_toggle)
        layout_toggle.addWidget(self.btn_toggle)
        grp_toggle.setLayout(layout_toggle)
        layout_setup.addWidget(grp_toggle)

        # 데이터 저장 / 데이터 불어오기
        grp_savedata = QGroupBox("SAVE DATA")
        layout_savedata = QHBoxLayout()
        self.btn_savedata = QPushButton("SAVE DATA")
        self.btn_savedata.setEnabled(False)
        self.btn_savedata.clicked.connect(self.save_data)

        self.btn_readdata = QPushButton("READ DATA")
        self.btn_readdata.clicked.connect(self.read_data)
        layout_savedata.addWidget(self.btn_savedata)
        layout_savedata.addWidget(self.btn_readdata)
        grp_savedata.setLayout(layout_savedata)
        layout_setup.addWidget(grp_savedata)

        # ZOOM Function
        grp_zoom = QGroupBox("ZOOM")
        layout_zoom = QHBoxLayout()

        self.btn_zoomA = QPushButton("ZOOM IN")
        self.btn_zoomA.setEnabled(False)
        self.btn_zoomA.clicked.connect(self.zoom_graph)
        layout_zoom.addWidget(self.btn_zoomA)

        self.btn_zoom6 = QPushButton("6 WINDOWS")
        self.btn_zoom6.setEnabled(False)
        self.btn_zoom6.clicked.connect(self.zoom_graph6)
        layout_zoom.addWidget(self.btn_zoom6)

        grp_zoom.setLayout(layout_zoom)
        layout_setup.addWidget(grp_zoom)

        # Logo Image
        labelLogo = QLabel("")
        pixmap = QPixmap(resource_path("logo.png"))
        labelLogo.setAlignment(Qt.AlignRight)
        labelLogo.setPixmap(pixmap)
        layout_setup.addWidget(labelLogo)        

        main_layer.addLayout(layout_setup)

    ## 2rd ########################
        # 그래프 범례
        grp_legend = QGroupBox("LEGEND")
        layout_legend = QHBoxLayout()

        # voltage1 
        grp_voltage1 = QGroupBox("")
        layout_voltage1 = QGridLayout()
        btn_voltage1 = QPushButton("OUT VOLTAGE1")
        self.checkbox_voltage1 = QCheckBox("")
        self.checkbox_voltage1.setChecked(True)
        self.checkbox_voltage1.stateChanged.connect(lambda:self.checkBoxChanged("OUT VOLTAGE1"))
        
        btn_voltage1.setStyleSheet("color: #ff0000;")
        btn_voltage1.clicked.connect(lambda:self.draw_graph_each('OUT VOLTAGE1', color=(255, 0, 0)))
        self.label_voltage1 = QLabel("0")
        self.label_voltage1.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        label_voltage1_unit = QLabel("V")
        # label_voltage1_unit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)

        layout_voltage1.addWidget(btn_voltage1, 0, 0, 1, 3)
        layout_voltage1.addWidget(self.checkbox_voltage1, 0, 4)
        layout_voltage1.addWidget(self.label_voltage1, 1, 0, 1, 3)
        layout_voltage1.addWidget(label_voltage1_unit, 1, 4)

        grp_voltage1.setLayout(layout_voltage1)
        layout_legend.addWidget(grp_voltage1)


        # current1 
        grp_current1 = QGroupBox("")
        layout_current1 = QGridLayout()
        btn_current1 = QPushButton("OUT CURRENT1")
        self.checkbox_current1 = QCheckBox("")
        self.checkbox_current1.setChecked(True)
        self.checkbox_current1.stateChanged.connect(lambda:self.checkBoxChanged("OUT CURRENT1"))
        
        btn_current1.setStyleSheet("color: #0000ff;")
        btn_current1.clicked.connect(lambda:self.draw_graph_each('OUT CURRENT1', color=(0, 0, 255)))
        self.label_current1 = QLabel("0")
        self.label_current1.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        label_current1_unit = QLabel("uA")
        # label_current1_unit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        
        layout_current1.addWidget(btn_current1, 0, 0, 1, 3)
        layout_current1.addWidget(self.checkbox_current1, 0, 4)
        layout_current1.addWidget(self.label_current1, 1, 0, 1, 3)
        layout_current1.addWidget(label_current1_unit, 1, 4)
        grp_current1.setLayout(layout_current1)
        layout_legend.addWidget(grp_current1)

        # voltage2 
        grp_voltage2 = QGroupBox("")
        layout_voltage2 = QGridLayout()
        btn_voltage2 = QPushButton("OUT VOLTAGE2")
        self.checkbox_voltage2 = QCheckBox("")
        self.checkbox_voltage2.setChecked(True)
        self.checkbox_voltage2.stateChanged.connect(lambda:self.checkBoxChanged("OUT VOLTAGE2"))
        
        btn_voltage2.setStyleSheet("color: #ffbf00;")
        btn_voltage2.clicked.connect(lambda:self.draw_graph_each('OUT VOLTAGE2', color=(255, 191, 0)))
        self.label_voltage2 = QLabel("0")
        self.label_voltage2.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        label_voltage2_unit = QLabel("V")
        # label_voltage2_unit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        
        layout_voltage2.addWidget(btn_voltage2, 0, 0, 1, 3)
        layout_voltage2.addWidget(self.checkbox_voltage2, 0, 4)
        layout_voltage2.addWidget(self.label_voltage2, 1, 0, 1, 3)
        layout_voltage2.addWidget(label_voltage2_unit, 1, 4)
        grp_voltage2.setLayout(layout_voltage2)
        layout_legend.addWidget(grp_voltage2)

        # current2 
        grp_current2 = QGroupBox("")
        layout_current2 = QGridLayout()
        btn_current2 = QPushButton("OUT CURRENT2")
        self.checkbox_current2 = QCheckBox("")
        self.checkbox_current2.setChecked(True)
        self.checkbox_current2.stateChanged.connect(lambda:self.checkBoxChanged("OUT CURRENT2"))
        
        btn_current2.setStyleSheet("color: #000080;")
        btn_current2.clicked.connect(lambda:self.draw_graph_each('OUT CURRENT2', color=(0,0,128)))
        self.label_current2 = QLabel("0")
        self.label_current2.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        label_current2_unit = QLabel("uA")
        # label_current2_unit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        
        layout_current2.addWidget(btn_current2, 0, 0, 1, 3)
        layout_current2.addWidget(self.checkbox_current2, 0, 4)
        layout_current2.addWidget(self.label_current2, 1, 0, 1, 3)
        layout_current2.addWidget(label_current2_unit, 1, 4)
        grp_current2.setLayout(layout_current2)
        layout_legend.addWidget(grp_current2)
        
        # vbia 
        grp_vbia = QGroupBox("")
        layout_vbia = QGridLayout()
        btn_vbia = QPushButton("VBIAS")
        self.checkbox_vbia = QCheckBox("")
        self.checkbox_vbia.setChecked(True)
        self.checkbox_vbia.stateChanged.connect(lambda:self.checkBoxChanged("VBIAS"))
        
        btn_vbia.setStyleSheet("color: #00ff00;")
        btn_vbia.clicked.connect(lambda:self.draw_graph_each('VBIAS', color=(0, 255, 0)))
        self.label_vbia = QLabel("0")
        self.label_vbia.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        label_vbia_unit = QLabel("V")
        # label_vbia_unit.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        
        layout_vbia.addWidget(btn_vbia, 0, 0, 1, 3)
        layout_vbia.addWidget(self.checkbox_vbia, 0, 4)
        layout_vbia.addWidget(self.label_vbia, 1, 0, 1, 3)
        layout_vbia.addWidget(label_vbia_unit, 1, 4)
        grp_vbia.setLayout(layout_vbia)
        layout_legend.addWidget(grp_vbia)

        # cps 
        grp_cps = QGroupBox("")
        layout_cps = QGridLayout()
        btn_cps = QPushButton("CPS")
        self.checkbox_cps = QCheckBox("")
        self.checkbox_cps.setChecked(True)
        self.checkbox_cps.stateChanged.connect(lambda:self.checkBoxChanged("CPS"))
        
        btn_cps.setStyleSheet("color: #cc9900;")
        btn_cps.clicked.connect(lambda:self.draw_graph_each('CPS', color=(204, 153, 0)))
        self.label_cps = QLabel("0")
        self.label_cps.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        label_cps_unit = QLabel("pF")
        label_cps_unit.setAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
        
        layout_cps.addWidget(btn_cps, 0, 0, 1, 3)
        layout_cps.addWidget(self.checkbox_cps, 0, 4)
        layout_cps.addWidget(self.label_cps, 1, 0, 1, 3)
        layout_cps.addWidget(label_cps_unit, 1, 4)
        grp_cps.setLayout(layout_cps)
        layout_legend.addWidget(grp_cps)

        # dechuckstart 
        grp_dechuckstart = QGroupBox("")
        layout_dechuckstart = QGridLayout()
        btn_dechuckstart = QPushButton("DECHUCK START")
        self.checkbox_dechuckstart = QCheckBox("")
        self.checkbox_dechuckstart.setChecked(True)
        self.checkbox_dechuckstart.stateChanged.connect(lambda:self.checkBoxChanged("DECHUCK START"))
        
        btn_dechuckstart.setStyleSheet("color: #cc0099;")
        btn_dechuckstart.clicked.connect(lambda:self.draw_graph_each('DECHUCK START', color=(204, 0, 153)))
        self.label_dechuckstart = QLabel("0")
        self.label_dechuckstart.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        
        layout_dechuckstart.addWidget(btn_dechuckstart, 0, 0, 1, 3)
        layout_dechuckstart.addWidget(self.checkbox_dechuckstart, 0, 4)
        layout_dechuckstart.addWidget(self.label_dechuckstart, 1, 0, 1, 3)
        grp_dechuckstart.setLayout(layout_dechuckstart)
        layout_legend.addWidget(grp_dechuckstart)

        # dechuckend 
        grp_dechuckend = QGroupBox("")
        layout_dechuckend = QGridLayout()
        btn_dechuckend = QPushButton("DECHUCK END")
        self.checkbox_dechuckend = QCheckBox("")
        self.checkbox_dechuckend.setChecked(True)
        self.checkbox_dechuckend.stateChanged.connect(lambda:self.checkBoxChanged("DECHUCK END"))
        
        btn_dechuckend.setStyleSheet("color: #6600cc;")
        btn_dechuckend.clicked.connect(lambda:self.draw_graph_each('DECHUCK END', color=(102, 0, 204)))
        self.label_dechuckend = QLabel("0")
        self.label_dechuckend.setAlignment(QtCore.Qt.AlignRight | QtCore.Qt.AlignVCenter)
        
        layout_dechuckend.addWidget(btn_dechuckend, 0, 0, 1, 3)
        layout_dechuckend.addWidget(self.checkbox_dechuckend, 0, 4)
        layout_dechuckend.addWidget(self.label_dechuckend, 1, 0, 1, 3)
        grp_dechuckend.setLayout(layout_dechuckend)
        layout_legend.addWidget(grp_dechuckend)

        grp_legend.setLayout(layout_legend)
        main_layer.addWidget(grp_legend)

    ## 2-1rd Fault Message ########
        grp_fault = QGroupBox("에러 메시지")
        layout_fault = QHBoxLayout()
        self.label_fault = QLabel("")
        layout_fault.addWidget(self.label_fault)
        grp_fault.setLayout(layout_fault)
        main_layer.addWidget(grp_fault)

    ## 3th ########################
        # 그래프 디스플레이
        grp_display = QGroupBox("GRAPH")
        layout_display = QHBoxLayout()
        self.graphWidget = pyGraph.PlotWidget()
        self.graphWidget.setBackground('w')
        
        ## create mainAxis
        self.mainAxis = self.graphWidget.plotItem
        self.mainAxis.setLabels(left='Voltage')
        self.mainAxis.showAxis('right')

        ## Second ViewBox
        self.axisB = pyGraph.ViewBox()
        self.mainAxis.scene().addItem(self.axisB)
        self.mainAxis.getAxis('right').linkToView(self.axisB)
        self.axisB.setXLink(self.mainAxis)
        self.mainAxis.getAxis('right').setLabel('Current', color='#0000ff')

        ## create Third ViewBox. 
        self.axisC = pyGraph.ViewBox()
        ax3 = pyGraph.AxisItem('right')
        self.mainAxis.layout.addItem(ax3, 2, 3)
        self.mainAxis.scene().addItem(self.axisC)
        ax3.linkToView(self.axisC)
        self.axisC.setXLink(self.mainAxis)
        ax3.setLabel('Vais', color='#00ff00')

        self.updateViews()
        self.mainAxis.vb.sigResized.connect(self.updateViews)

        self.graph_voltage1s = self.mainAxis.plot(self.temp_indexs, self.temp_voltage1s, pen=self.pen_voltage1, name="Voltage1s")
        self.graph_voltage2s = self.mainAxis.plot(self.temp_indexs, self.temp_voltage2s, pen=self.pen_voltage2, name="voltage2s")
        self.graph_cpss = self.mainAxis.plot(self.temp_indexs, self.temp_cpss, pen=self.pen_cps, name="Cps")
        self.graph_dechuckstarts = self.mainAxis.plot(self.temp_indexs, self.temp_dechuckstarts, pen=self.pen_dechuckstart, name="Dechuck_start")
        
        self.itemCurrent1 = pyGraph.PlotCurveItem(self.temp_indexs, self.temp_current1s, pen=self.pen_current1, name="Current1s")
        self.graph_current1s = self.axisB.addItem(self.itemCurrent1)
        
        self.itemCurrent2 = pyGraph.PlotCurveItem(self.temp_indexs, self.temp_current2s, pen=self.pen_current2, name="Current2s")
        self.graph_current2s = self.axisB.addItem(self.itemCurrent2)

        self.itemVbia = pyGraph.PlotCurveItem(self.temp_indexs, self.temp_vbias, pen=self.pen_vbia, name="Vbias")
        self.graph_vbias = self.axisC.addItem(self.itemVbia)
        self.itemDechuckend = pyGraph.PlotCurveItem(self.temp_indexs, self.temp_dechuckends, pen=self.pen_dechuckend, name="Dechuck_end")
        self.graph_dechuckends = self.axisC.addItem(self.itemDechuckend)

        layout_display.addWidget(self.graphWidget)
        grp_display.setLayout(layout_display)
        main_layer.addWidget(grp_display)
    
    ## 4th ########################
        # Status
        self.statusbar = QStatusBar()
        self.setStatusBar(self.statusbar)
        self.statusbar.setObjectName("statusbar")
        
        self.statusmessage = 'PSTEK, {},  {}'
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), 'Ready !!')
        self.statusbar.showMessage(displaymessage)

        # self.showFullScreen()

    # Graph Update
    def updateViews(self):
        ## view has resized; update auxiliary views to match
        # global self.mainAxis, self.axisB, self.axisC
        self.axisB.setGeometry(self.mainAxis.vb.sceneBoundingRect())
        self.axisC.setGeometry(self.mainAxis.vb.sceneBoundingRect())
        self.axisB.linkedViewChanged(self.mainAxis.vb, self.axisB.XAxis)
        self.axisC.linkedViewChanged(self.mainAxis.vb, self.axisC.XAxis)

    # CheckBox clicked
    def checkBoxChanged(self, category):
        if category == 'OUT VOLTAGE1':
            if self.checkbox_voltage1.isChecked() == True:
                self.view_voltage1 = True
                self.graph_voltage1s.show()
            else:
                self.view_voltage1 = False
                self.graph_voltage1s.hide()

        elif category == 'OUT CURRENT1':
            if self.checkbox_current1.isChecked() == True:
                self.view_current1 = True
                self.axisB.addItem(self.itemCurrent1)
            else:
                self.view_current1 = False 
                self.axisB.removeItem(self.itemCurrent1)

        elif category == 'OUT VOLTAGE2':
            if self.checkbox_voltage2.isChecked() == True:
                self.view_voltage2 = True
                self.graph_voltage2s.show()
            else:
                self.view_voltage2 = False 
                self.graph_voltage2s.hide()

        elif category == 'OUT CURRENT2':
            if self.checkbox_current2.isChecked() == True:
                self.view_current2 = True
                self.axisB.addItem(self.itemCurrent2)
            else:
                self.view_current2 = False 
                self.axisB.removeItem(self.itemCurrent2)

        elif category == 'VBIAS':
            if self.checkbox_vbia.isChecked() == True:
                self.view_vbia = True
                self.axisC.addItem(self.itemVbia)
            else:
                self.view_vbia = False 
                self.axisC.removeItem(self.itemVbia)

        elif category == 'CPS':
            if self.checkbox_cps.isChecked() == True:
                self.view_cps = True
                self.graph_cpss.show()
            else:
                self.view_cps = False  
                self.graph_cpss.hide()

        elif category == 'DECHUCK START':
            if self.checkbox_dechuckstart.isChecked() == True:
                self.view_dechuckstart = True
                self.graph_dechuckstarts.show()
            else:
                self.view_dechuckstart = False 
                self.graph_dechuckstarts.hide()

        elif category == 'DECHUCK END':
            if self.checkbox_dechuckend.isChecked() == True:
                self.view_dechuckend = True
                self.axisC.addItem(self.itemDechuckend)
            else:
                self.view_dechuckend = False  
                self.axisC.removeItem(self.itemDechuckend)
        else:
            print(category, "No Category")

    # SAVE DATA as JSON FILE
    def save_data(self):
        data = {}
        Dialog = QDialog()
        self.com_open_flag == True
        dialog = DataWin(Dialog)
        dialog.show()
        response = dialog.exec_()

        time_list = []

        for tm in self.times:
            if isinstance(tm, str):
                time_list.append(tm)
            else:
                tm = time.localtime(tm)
                string = time.strftime('%I:%M:%S', tm)
                time_list.append(string)

        if response == QDialog.Accepted:
            dataset = pd.DataFrame({'indexs': self.indexs.tolist(),
                        "times": time_list,
                        'voltage1s': self.voltage1s.tolist(),
                        'current1s': self.current1s.tolist(),
                        'voltage2s': self.voltage2s.tolist(),
                        'current2s': self.current2s.tolist(),
                        'vbias': self.vbias.tolist(),
                        'vcss': self.vcss.tolist(),
                        'icss': self.icss.tolist(),
                        'cpss': self.cpss.tolist(),
                        'dechuckstarts': self.dechuckstarts.tolist(),
                        'dechuckends': self.dechuckends.tolist(),
            })

            product = dialog.text_product.text()
            user = dialog.text_user.text()

            now = str(time.time()).split('.')[0]
            subdir = os.path.join('.', 'output')
            if not os.path.exists(subdir):
                os.makedirs(subdir)

            # CSS FILE
            csvfile = f"esc_{product}_{user}_{now}.csv"
            csvfile = os.path.join(subdir, csvfile)
            dataset.to_csv(csvfile, sep=',')

            Nindexs = self.indexs.tolist()
            Times = time_list
            NVoltage1s = self.voltage1s.tolist()
            NCurrent1s = self.current1s.tolist()
            NVoltage2s = self.voltage2s.tolist()
            NCurrent2s = self.current2s.tolist()
            NVbias = self.vbias.tolist()

            NVcss = self.vcss.tolist()
            NIcss = self.icss.tolist()
            NCpss = self.cpss.tolist()
            NDechuck_starts = self.dechuckstarts.tolist()
            NNDechuck_ends = self.dechuckends.tolist()

            data['product'] = dialog.text_product.text()
            data['user'] = dialog.text_user.text()
            
            data['indexs'] = Nindexs
            data['times'] = Times
            data['voltage1s'] = NVoltage1s
            data['current1s'] = NCurrent1s
            data['voltage2s'] = NVoltage2s
            data['current2s'] = NCurrent2s
            data['vbias'] = NVbias
            data['vcss'] = NVcss
            data['icss'] = NIcss
            data['cpss'] = NCpss
            data['dechuckstarts'] = NDechuck_starts
            data['dechuckends'] = NNDechuck_ends
            
            # JSON FILE
            jsonfile = f"esc_{product}_{user}_{now}.json"
            jsonfile = os.path.join(subdir, jsonfile)
            with open(jsonfile, 'wb') as file:
                json.dump(data, codecs.getwriter('utf-8')(file))

    # Read File DATA (JSON)
    def read_data(self):
        fname, _ = QFileDialog.getOpenFileName(self, 'Open file', 
         '.',"Json files (*.json)")
        with open(fname, 'r', encoding='utf-8') as json_file:
            data = json.load(json_file)
            self.indexs = data['indexs']
            self.times = data['times']
            # print(self.times)
            self.voltage1s = data['voltage1s']
            self.current1s = data['current1s']
            self.voltage2s = data['voltage2s']
            self.current2s = data['current2s']
            self.vbias = data['vbias']
            self.vcss = data['vcss']
            self.icss = data['icss']
            self.cpss = data['cpss']

            try:
                self.dechuckstarts = data['dechuckstarts']
            except:
                self.dechuckstarts = data['dechuck_starts']
            
            try:
                self.dechuckends = data['dechuckends']
            except:
                self.dechuckends = data['dechuck_ends']

            self.temp_indexs = data['indexs']
            self.temp_times = data['times']
            self.temp_voltage1s = data['voltage1s']
            self.temp_current1s = data['current1s']
            self.temp_voltage2s = data['voltage2s']
            self.temp_current2s = data['current2s']
            self.temp_vbias = data['vbias']
            self.temp_vcss = data['vcss']
            self.temp_icss = data['icss']
            self.temp_cpss = data['cpss']
            
            try:
                self.temp_dechuckstarts = data['dechuckstarts']
            except:
                self.temp_dechuckstarts = data['dechuck_starts']
            
            try:
                self.temp_dechuckends = data['dechuckends']
            except:
                self.temp_dechuckends = data['dechuck_ends']

            product =  data.get('product')
            user =  data.get('user', '이름없음')

            title = "PRODUCT : {} / WRITER : {}".format(product, user)

        self.graphWidget.setTitle(title, color="b", size="20pt")

        self.btn_savedata.setEnabled(False)
        self.btn_zoomA.setEnabled(True)
        self.btn_zoom6.setEnabled(True)

        self.draw_graph()

    # RESET 
    def reset_data(self):
        self.countN = 0
        
        # Reser Data
        self.indexs = np.array([])
        self.times = np.array([])
        self.voltage1s = np.array([])
        self.current1s = np.array([])
        self.voltage2s = np.array([])
        self.current2s = np.array([])
        self.vbias = np.array([])
        self.vcss = np.array([])
        self.icss = np.array([])
        self.cpss = np.array([])
        self.dechuckstarts = np.array([])
        self.dechuckends = np.array([])

        # Reser Temp Data
        self.temp_indexs = np.array([])
        self.temp_times = np.array([])
        self.temp_voltage1s = np.array([])
        self.temp_current1s = np.array([])
        self.temp_voltage2s = np.array([])
        self.temp_current2s = np.array([])
        self.temp_vbias = np.array([])
        self.temp_vcss = np.array([])
        self.temp_icss = np.array([])
        self.temp_cpss = np.array([])
        self.temp_dechuckstarts = np.array([])
        self.temp_dechuckends = np.array([])

        # Flag Value Reset
        self.view_voltage1 = True
        self.view_current1 = True
        self.view_voltage2 = True
        self.view_current2 = True
        self.view_vbia = True
        self.view_cps = True
        self.view_dechuckstart = True
        self.view_dechuckend = True

        # Reset 그림 그리기
        self.draw_graph()

    # Graph 개별 그래프 
    def draw_graph_each(self, category, color):
        if self.graph_flag == False:
            self.graph_flag = True
            Dialog = QDialog()
            if category == 'OUT VOLTAGE1':
                values = self.voltage1s
            elif category == 'OUT CURRENT1':
                values = self.current1s
            elif category == 'OUT VOLTAGE2':
                values = self.voltage2s
            elif category == 'OUT CURRENT2':
                values = self.current2s
            elif category == 'VBIAS':
                values = self.vbias
            elif category == 'CPS':
                values = self.cpss
            elif category == 'DECHUCK START':
                values = self.dechuckstarts
            elif category == 'DECHUCK END':
                values = self.dechuckends

            dialog = GraphWindow(Dialog, category, color, self.indexs, values)
            dialog.show()
            response = dialog.exec_()

            if response == QDialog.Accepted or response == QDialog.Rejected:
                self.graph_flag = False

    # zoom_graph
    def zoom_graph(self):
        Dialog = QDialog()
        dialog = ZoomWindow(Dialog, self.indexs, self.voltage1s, self.current1s, self.voltage2s, self.current2s, self.vbias, self.cpss, self.dechuckstarts, self.dechuckends)
        dialog.show()
        response = dialog.exec_()

        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False

    # zoom_graph8
    def zoom_graph6(self):
        Dialog = QDialog()
        dialog = ZoomWindow6(Dialog, self.indexs, self.voltage1s, self.current1s, self.voltage2s, self.current2s, self.vbias, self.cpss, self.dechuckstarts, self.dechuckends)
        dialog.show()
        response = dialog.exec_()

        if response == QDialog.Accepted or response == QDialog.Rejected:
            self.graph_flag = False

    # 전원 장치와 송신을 위한 설정
    def setting_generator(self):
        if self.com_open_flag == False:
            Dialog = QDialog()
            self.com_open_flag == True
            dialog = SettingWin(Dialog)
            dialog.show()
            response = dialog.exec_()

            # OK 를 하면 설정 값을 읽어와서 통신을 한다.
            if response == QDialog.Accepted:
                self.gen_port = dialog.gen_port
                self.com_speed = dialog.com_speed
                self.com_data = dialog.com_data
                self.com_parity = dialog.com_parity
                self.com_stop = dialog.com_stop
                self.com_open_flag = False
                self.client = device.make_connect(
                    port=self.gen_port, ptype='rtu',
                    speed=self.com_speed, bytesize=self.com_data, 
                    parity=self.com_parity, stopbits=self.com_stop
                )

                if self.client:
                    self.label_gen_port.setText(self.gen_port)
                    # self.btn_com_gen.setEnabled(False)
                    self.btn_com_gen.hide()
                    # self.btn_pulldata.setEnabled(True)
                    self.btn_du_run.setEnabled(True)
                    self.btn_communication.setEnabled(True)
                    
                time.sleep(2)
                self.initial = device.read_setting_value(self.client)
                print(self.initial)
                self.toggle = self.initial['rd_toggle']

        else:
            print("Open Dialog")

    # 전원 장치와 송신을 위한 설정
    def device_setting_modify(self):
        Dialog = QDialog()
        if not self.initial:
            self.initial = device.read_setting_value(self.client)

        dialog = ESCComm(Dialog, self.client, self.initial)
        dialog.show()
        response = dialog.exec_()
        if response == QDialog.Accepted:
            self.time_delay = int(dialog.time_delay.value())

    # 전원 장치 Toggle
    def device_toggle(self):
        if self.toggle == device.FORWARD:
            self.toggle = device.REVERSE
            device.write_registers(self.client, device.WRITE_TOGGLE, device.REVERSE)

        else:
            self.toggle = device.FORWARD
            device.write_registers(self.client, device.WRITE_TOGGLE, device.FORWARD)

    # Bit return 함수 
    def bit_check(self, x):
        data = []
        for idx in range(15):
            if (x & (1<<idx)):
                data.append(idx)
        return data


    # Falult 오류 확인 
    def fault_check(self):
        x = device.read_fault(self.client)
        faults = self.bit_check(x)

        if faults:
            self.faulttimer.stop()
            self.timer.stop()
            self.stoptimer.stop()

            message = ""

            for fault in faults:
                message += self.faultmessage[fault]

            self.label_fault.setText(message)


    # Run Device (중간에 Run 을 눌러서 디바이스 시작시킴)
    def run_device(self):
        # 전원 장치를 시작하고, 데이터를 읽어옴
        # device.device_run(self.client)

        # 데이터를 가져오기 시작하면 STOP 버턴 활성화
        self.btn_zoomA.setEnabled(True)
        self.btn_zoom6.setEnabled(True)
        self.btn_du_stop.setEnabled(True)
        self.btn_toggle.setEnabled(True)
        self.stopFlag = False

        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), "DEVICE RUN")
        self.statusbar.showMessage(displaymessage)

        # setting timer
        self.timer.setInterval(100)
        self.timer.timeout.connect(self.get_data)
        self.timer.start()


    # 데이터 전송 중단
    def stop_data(self):
        self.btn_savedata.setEnabled(True)
        self.timer.stop()
        self.stoptimer.stop()
        now = QDate.currentDate()
        displaymessage = self.statusmessage.format(now.toString(Qt.ISODate), "DEVICE STOP")
        self.statusbar.showMessage(displaymessage)


    # 데이터 가져 오기
    def get_data(self):
        # result = device.read_registers(self.client, READ_ADDRESS_VOLTAGE1, 10)
        result = device.read_registers_simu(self.client, device.READ_ADDRESS_VOLTAGE1, 10)
        if result:
            self.countN = self.countN  + 1
            tm = time.time()

            voltage1 = c_int16(result[0]).value
            current1 = result[1] #* 100
            voltage2 = c_int16(result[2]).value
            current2 = result[3] #* 100
            vbia = c_int16(result[4]).value

            # Label 에 데이터 보여주기
            self.label_voltage1.setText(str(voltage1))
            self.label_current1.setText(str(result[1]))
            self.label_voltage2.setText(str(voltage2))
            self.label_current2.setText(str(result[3]))
            
            self.label_vbia.setText(str(vbia))
            self.label_cps.setText(str(result[7]))
            self.label_dechuckstart.setText(str(result[8]))
            self.label_dechuckend.setText(str(result[9]))

            # 데이터 값을 Array 에 저장하기 
            self.indexs = np.append(self.indexs, self.countN)
            self.times = np.append(self.times, tm)
            self.voltage1s = np.append(self.voltage1s, voltage1)
            self.current1s = np.append(self.current1s, current1)
            self.voltage2s = np.append(self.voltage2s, voltage2)
            self.current2s = np.append(self.current2s, current2)
            self.vbias = np.append(self.vbias, vbia)

            self.vcss = np.append(self.vcss, result[5])
            self.icss = np.append(self.icss, result[6])
            self.cpss = np.append(self.cpss, result[7])
            self.dechuckstarts = np.append(self.dechuckstarts, result[8])
            self.dechuckends = np.append(self.dechuckends, result[9])

            # 데이터 값 임시 값에 저장하기 Display 용
            self.temp_indexs = np.append(self.temp_indexs, self.countN)
            self.temp_times = np.append(self.temp_times, tm)
            self.temp_voltage1s = np.append(self.temp_voltage1s, voltage1)
            self.temp_current1s = np.append(self.temp_current1s, current1)
            self.temp_voltage2s = np.append(self.temp_voltage2s, voltage2)
            self.temp_current2s = np.append(self.temp_current2s, current2)
            self.temp_vbias = np.append(self.temp_vbias, vbia)

            self.temp_vcss = np.append(self.temp_vcss, result[5])
            self.temp_icss = np.append(self.temp_icss, result[6])
            self.temp_cpss = np.append(self.temp_cpss, result[7])
            self.temp_dechuckstarts = np.append(self.temp_dechuckstarts, result[8])
            self.temp_dechuckends = np.append(self.temp_dechuckends, result[9])

            if self.countN >= 200 and self.countN % 10 == 0:
                self.temp_indexs = self.temp_indexs[10:]
                self.temp_times = self.temp_times[10:]
                self.temp_voltage1s = self.temp_voltage1s[10:]
                self.temp_current1s = self.temp_current1s[10:]
                self.temp_voltage2s = self.temp_voltage2s[10:]
                self.temp_current2s = self.temp_current2s[10:]
                self.temp_vbias = self.temp_vbias[10:]

                self.temp_vcss = self.temp_vcss[10:]
                self.temp_icss = self.temp_icss[10:]
                self.temp_cpss = self.temp_cpss[10:]
                self.temp_dechuckstarts = self.temp_dechuckstarts[10:]
                self.temp_dechuckends = self.temp_dechuckends[10:]

            if self.countN % 10 == 0:
                self.draw_graph()

    # draw_graph using all graph
    def draw_graph(self):
        self.updateViews()
        self.mainAxis.vb.sigResized.connect(self.updateViews)
        if self.view_voltage1:
            self.graph_voltage1s.hide()
            self.graph_voltage1s =  self.mainAxis.plot(self.temp_voltage1s, pen=self.pen_voltage1, name="Voltage1s")
            self.graph_voltage1s.show()
        else:
            self.graph_voltage1s.hide()

        if self.view_current1:
            self.axisB.removeItem(self.itemCurrent1)
            self.itemCurrent1 = pyGraph.PlotCurveItem(self.temp_current1s, pen=self.pen_current1, name="Current1s")
            self.graph_current1s = self.axisB.addItem(self.itemCurrent1)
        else:
            self.axisB.removeItem(self.itemCurrent1)

        if self.view_voltage2:
            self.graph_voltage2s.hide()
            self.graph_voltage2s =  self.mainAxis.plot(self.temp_voltage2s, pen=self.pen_voltage2, name="voltage2s")
            self.graph_voltage2s.show()
        else:
            self.graph_voltage2s.hide()

        if self.view_current2:
            self.axisB.removeItem(self.itemCurrent2)
            self.itemCurrent2 = pyGraph.PlotCurveItem(self.temp_current2s, pen=self.pen_current2, name="Current2s")
            self.graph_current2s = self.axisB.addItem(self.itemCurrent2)
        else:
            self.axisB.removeItem(self.itemCurrent2)

        if self.view_vbia:
            self.axisC.removeItem(self.itemVbia)
            self.itemVbia = pyGraph.PlotCurveItem(self.temp_vbias, pen=self.pen_vbia, name="Vbias")
            self.graph_vbias = self.axisC.addItem(self.itemVbia)
        else:
            self.axisC.removeItem(self.itemVbia)

        if self.view_cps:
            self.graph_cpss.hide()
            self.graph_cpss =  self.mainAxis.plot(self.temp_cpss, pen=self.pen_cps, name="Cps")
            self.graph_cpss.show()
        else:
            self.graph_cpss.hide()

        if self.view_dechuckstart:
            self.graph_dechuckstarts.hide()
            self.graph_dechuckstarts =  self.mainAxis.plot(self.temp_dechuckstarts, pen=self.pen_dechuckstart, name="Dechuck_start")
            self.graph_dechuckstarts.show()
        else:
            self.graph_dechuckstarts.hide()

        if self.view_dechuckend:
            self.axisC.removeItem(self.itemDechuckend)
            self.itemDechuckend = pyGraph.PlotCurveItem(self.temp_dechuckends, pen=self.pen_dechuckend, name="Dechuck_end")
            self.graph_dechuckends = self.axisC.addItem(self.itemDechuckend)
        else:
            self.axisC.removeItem(self.itemDechuckend)


    # 단말기 STOP
    def device_stop(self):
        # device.device_stop(self.client)
        self.stopFlag = True

        # self.get_data()
        self.stoptimer.setInterval(self.time_delay * 1000)
        self.stoptimer.timeout.connect(self.stop_data)
        self.stoptimer.start()

    # Keyboard Event
    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Escape:
            pass
        elif e.key() == Qt.Key_F:
            self.showFullScreen()
        elif e.key() == Qt.Key_N:
            self.showNormal()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    form = MainWindow()
    form.show()
    sys.exit(app.exec_())