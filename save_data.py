#!/usr/bin/env python
# coding: utf-8

from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui, QtWidgets

class DataWin(QDialog):
    def __init__(self, Dialog):
        super().__init__()
        self.gtype = '20kHz'
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        self.setGeometry(100, 100, 500, 250)
        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

        # 고객사 이름 
        group_customer = QGroupBox("")
        layout_customer = QVBoxLayout()
        label_customer = QLabel("고객사 명")
        self.text_customer = QLineEdit("훼스텍")
        layout_customer.addWidget(label_customer)
        layout_customer.addWidget(self.text_customer)
        group_customer.setLayout(layout_customer)
        main_layer.addWidget(group_customer)

        # 전원 장치 구분
        group_generator = QGroupBox("")
        layout_main_generator = QVBoxLayout()
        
        label_generator = QLabel("전원장치 출력(15/20kHz)")
        layout_main_generator.addWidget(label_generator)

        group_gtype = QGroupBox("")
        layout_gtype = QHBoxLayout()
        self.rd_gtype_20 = QRadioButton("20kHz")
        self.rd_gtype_20.setChecked(True)
        self.rd_gtype_20.clicked.connect(lambda:self.radioValueClicked(self.rd_gtype_20))
        layout_gtype.addWidget(self.rd_gtype_20)

        self.rd_gtype_15 = QRadioButton("15kHz")
        self.rd_gtype_15.clicked.connect(lambda:self.radioValueClicked(self.rd_gtype_15))
        layout_gtype.addWidget(self.rd_gtype_15)
        group_gtype.setLayout(layout_gtype)
        layout_main_generator.addWidget(group_gtype)
        
        group_generator.setLayout(layout_main_generator)
        main_layer.addWidget(group_generator)

        # 고객사 이름 
        group_serial = QGroupBox("")
        layout_serial = QVBoxLayout()
        label_serial = QLabel("제품(혼) 번호(Serial)")
        self.text_serial = QLineEdit("")
        layout_serial.addWidget(label_serial)
        layout_serial.addWidget(self.text_serial)
        group_serial.setLayout(layout_serial)
        main_layer.addWidget(group_serial)

        # 등록자 명
        group_user = QGroupBox("")
        layout_user = QVBoxLayout()
        label_user = QLabel("등록자명")
        self.text_user = QLineEdit("배순철전무")
        layout_user.addWidget(label_user)
        layout_user.addWidget(self.text_user)
        group_user.setLayout(layout_user)
        main_layer.addWidget(group_user)

        
        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        main_layer.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)

    def radioValueClicked(self, btn):
        value = btn.text()
        self.gtype = value


    def on_accepted(self):
        self.accept()


if __name__ == '__main__':
    import sys
    app = QApplication(sys.argv)
    Dialog = QDialog()
    login = LoginWin(Dialog)
    login.show()

    response = login.exec_()

    if response == QDialog.Accepted:
        print("QDialog.Accepted")
        sys.exit(app.exec_())