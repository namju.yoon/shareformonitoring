import sys  # We need sys so that we can pass argv to QApplication
import os
import time
import pyqtgraph as pyGraph

from PyQt5.QtWidgets import QDialog, QHBoxLayout, QDialogButtonBox, QDialogButtonBox
from PyQt5.QtWidgets import QDialogButtonBox, QApplication, QVBoxLayout, QDesktopWidget
from PyQt5 import QtCore
from pyqtgraph import PlotWidget, plot


class GraphWindow(QDialog):

    def __init__(self, Dialog, title, color, x, y):
        super().__init__()
        self.title = title
        self.x = x 
        self.y = y
        self.color = color
        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        # self.setGeometry(100, 100, 1200, 800)
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        main_layer = QVBoxLayout()
        self.setWindowTitle("GRAPH FOR EACH")
        self.setLayout(main_layer)

        self.graphWidget = pyGraph.PlotWidget()
        main_layer.addWidget(self.graphWidget)

        self.graphWidget.setBackground('w')
        self.graphWidget.setTitle(self.title, color=self.color, size="30pt")
    
        self.plot(self.x, self.y, self.color)

        self.buttonBox = QDialogButtonBox()
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        main_layer.addWidget(self.buttonBox)

        self.buttonBox.accepted.connect(self.on_accepted)
        self.buttonBox.rejected.connect(self.reject)


    def plot(self, x, y, color):
        pen = pyGraph.mkPen(color=color, width=2)
        self.graphWidget.plot(x, y, pen=pen, symbolSize=10, symbolBrush=('b'))

    def on_accepted(self):
        self.accept()

class ZoomWindow(QDialog):
    def __init__(self, Dialog, indexs, voltage1s, current1s, voltage2s, current2s, vbias, cpss, dechuck_starts, dechuck_ends):
        super().__init__()
        self.title = "ZOOM IN DATA"
        self.indexs = indexs
        self.voltage1s = voltage1s 
        self.current1s = current1s 
        self.voltage2s = voltage2s 
        self.current2s = current2s 
        self.vbias = vbias 
        self.cpss = cpss 
        self.dechuck_starts = dechuck_starts 
        self.dechuck_ends = dechuck_ends 

        # print(self.time_list)

        self.pen_voltage1 = pyGraph.mkPen(width=1, color=(255, 0, 0))
        self.pen_current1 = pyGraph.mkPen(width=1, color=(0, 0, 255))
        self.pen_voltage2 = pyGraph.mkPen(width=1, color=(255, 191, 0))
        self.pen_current2 = pyGraph.mkPen(width=1, color=(0,0,128))
        self.pen_vbia = pyGraph.mkPen(width=1, color=(0, 255, 0))
        self.pen_cps = pyGraph.mkPen(width=1, color=(204, 153, 0))
        self.pen_dechuck_start = pyGraph.mkPen(width=1, color=(204, 0, 153))
        self.pen_dechuck_end = pyGraph.mkPen(width=1, color=(102, 0, 204))

        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        main_layer = QVBoxLayout()
        self.setWindowTitle("ZOOM")
        self.setLayout(main_layer)

        self.main = pyGraph.PlotWidget()
        self.main.setBackground('w')
        self.sub = pyGraph.PlotWidget()
        self.sub.setBackground('w')

        pyGraph.setConfigOptions(antialias=True)
        main_layer.addWidget(self.main)
        main_layer.addWidget(self.sub)

    ## create mainAxis
        # First ViewBox
        self.mainWindow = self.main.plotItem
        self.mainWindow.setLabels(left='Voltage')
        self.mainWindow.showAxis('right')

        ## Second ViewBox
        self.axisB = pyGraph.ViewBox()
        self.axisB.setXLink(self.mainWindow)

        self.mainWindow.scene().addItem(self.axisB)
        self.mainWindow.getAxis('right').linkToView(self.axisB)
        self.mainWindow.getAxis('right').setLabel('Current', color='#0000ff')

        ## create Third ViewBox. 
        self.axisC = pyGraph.ViewBox()
        ax3 = pyGraph.AxisItem('right')
        self.mainWindow.layout.addItem(ax3, 2, 3)
        self.mainWindow.scene().addItem(self.axisC)
        ax3.linkToView(self.axisC)
        self.axisC.setXLink(self.mainWindow)
        ax3.setLabel('Vais', color='#00ff00')

        ## Handle view resizing 
        def updateViews():
            ## view has resized; update auxiliary views to match
            # global self.mainWindow, self.axisB, self.axisC
            self.axisB.setGeometry(self.mainWindow.vb.sceneBoundingRect())
            self.axisC.setGeometry(self.mainWindow.vb.sceneBoundingRect())
            
            self.axisB.linkedViewChanged(self.mainWindow.vb, self.axisB.XAxis)
            self.axisC.linkedViewChanged(self.mainWindow.vb, self.axisC.XAxis)

        updateViews()
        self.mainWindow.vb.sigResized.connect(updateViews)

        self.mainWindow.plot(self.indexs, self.voltage1s, pen=self.pen_voltage1, name="Voltage1s")
        self.mainWindow.plot(self.indexs, self.voltage2s, pen=self.pen_voltage2, name="voltage2s")
        self.axisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current2s, pen=self.pen_current2, name="Current2s"))
        self.axisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current1s, pen=self.pen_current1, name="Current1s"))

        self.mainWindow.plot(self.indexs, self.cpss, pen=self.pen_cps, name="Cps")
        self.mainWindow.plot(self.indexs, self.dechuck_starts, pen=self.pen_dechuck_start, name="Dechuck_start")
        self.axisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.vbias, pen=self.pen_vbia, name="Vbias"))
        self.axisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.dechuck_ends, pen=self.pen_dechuck_end, name="Dechuck_end"))

        linZone = pyGraph.LinearRegionItem([100,200])
        linZone.setZValue(-10)
        self.mainWindow.addItem(linZone)

        ## create subWin
        # First ViewBox
        self.subWin = self.sub.plotItem
        self.subWin.setLabels(left='Voltage')
        self.subWin.showAxis('right')

    ## Second ViewBox
        self.subxisB = pyGraph.ViewBox()
        self.subxisB.setXLink(self.subWin)

        self.subWin.scene().addItem(self.subxisB)
        self.subWin.getAxis('right').linkToView(self.subxisB)
        self.subWin.getAxis('right').setLabel('Current', color='#0000ff')

        ## create Third ViewBox. 
        self.subxisC = pyGraph.ViewBox()
        subax3 = pyGraph.AxisItem('right')
        self.subWin.layout.addItem(subax3, 2, 3)
        self.subWin.scene().addItem(self.subxisC)
        subax3.linkToView(self.subxisC)
        self.subxisC.setXLink(self.subWin)
        subax3.setLabel('Vais', color='#00ff00')

        def updateSubViews():
            ## view has resized; update auxiliary views to match
            # global self.mainWindow, self.subxisB, self.subxisC
            self.subxisB.setGeometry(self.subWin.vb.sceneBoundingRect())
            self.subxisC.setGeometry(self.subWin.vb.sceneBoundingRect())
            
            self.subxisB.linkedViewChanged(self.subWin.vb, self.subxisB.XAxis)
            self.subxisC.linkedViewChanged(self.subWin.vb, self.subxisC.XAxis)

        updateSubViews()
        self.mainWindow.vb.sigResized.connect(updateSubViews)

        self.subWin.plot(self.indexs, self.voltage1s, pen=self.pen_voltage1, name="Voltage1s")
        self.subWin.plot(self.indexs, self.voltage2s, pen=self.pen_voltage2, name="voltage2s")
        self.subWin.plot(self.indexs, self.cpss, pen=self.pen_cps, name="Cps")
        self.subWin.plot(self.indexs, self.dechuck_starts, pen=self.pen_dechuck_start, name="Dechuck_start")
        
        self.subxisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current1s, pen=self.pen_current1, name="Current1s"))
        self.subxisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current2s, pen=self.pen_current2, name="Current2s"))

        self.subxisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.vbias, pen=self.pen_vbia, name="Vbias"))
        self.subxisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.dechuck_ends, pen=self.pen_dechuck_end, name="Dechuck_end"))

        # Region 이 변경되면 오른쪽 그래프를 변경해 줌
        def updateChangePlot():
            self.subWin.setXRange(*linZone.getRegion(), padding=0)

        def updateRegion():
            linZone.setRegion(self.subWin.getViewBox().viewRange()[0])

        linZone.sigRegionChanged.connect(updateChangePlot)
        self.subWin.sigXRangeChanged.connect(updateRegion)

        updateChangePlot()

    def on_accepted(self):
        self.accept()


class ZoomWindow6(QDialog):

    def __init__(self, Dialog, indexs, voltage1s, current1s, voltage2s, current2s, vbias, cpss, dechuck_starts, dechuck_ends):
        super().__init__()
        self.title = "ZOOM IN DATA"
        self.indexs = indexs 
        self.voltage1s = voltage1s 
        self.current1s = current1s 
        self.voltage2s = voltage2s 
        self.current2s = current2s 
        self.vbias = vbias 
        self.cpss = cpss 
        self.dechuck_starts = dechuck_starts 
        self.dechuck_ends = dechuck_ends 

        self.pen_voltage1 = pyGraph.mkPen(width=1, color=(255, 0, 0))
        self.pen_current1 = pyGraph.mkPen(width=1, color=(0, 0, 255))
        self.pen_voltage2 = pyGraph.mkPen(width=1, color=(255, 191, 0))
        self.pen_current2 = pyGraph.mkPen(width=1, color=(0,255, 0))
        self.pen_vbia = pyGraph.mkPen(width=1, color=(0, 255, 0))
        self.pen_cps = pyGraph.mkPen(width=1, color=(204, 153, 0))
        self.pen_dechuck_start = pyGraph.mkPen(width=1, color=(204, 0, 153))
        self.pen_dechuck_end = pyGraph.mkPen(width=1, color=(102, 0, 204))

        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        screensize = QDesktopWidget().screenGeometry(-1)
        self.setMinimumSize(screensize.width(), screensize.height()) 

        main_layer = QVBoxLayout()
        self.setWindowTitle("6 Windows")
        self.setLayout(main_layer)

        self.window = pyGraph.GraphicsLayoutWidget(show=True, title='8 Window Plot')
        pyGraph.setConfigOptions(antialias=True)
        main_layer.addWidget(self.window)

        voltage_plot = self.window.addPlot(title='OUT VOLTAGE 1, 2')
        voltage_plot.plot(x=self.indexs, y=self.voltage1s, pen=self.pen_voltage1, name="OUT VOLTAGE1")
        voltage_plot.plot(x=self.indexs, y=self.voltage2s, pen=self.pen_voltage2, name="OUT VOLTAGE2")

        current_plot = self.window.addPlot(title='OUT CURRENT 1, 2')
        current_plot.plot(x=self.indexs, y=self.current1s, pen=self.pen_current1, name="OUT CURRENT1")
        current_plot.plot(x=self.indexs, y=self.current2s, pen=self.pen_current2, name="OUT CURRENT2")

        self.window.nextRow()

        vias = self.window.addPlot(title='VBIAS')
        vias.plot(x=self.indexs, y=self.vbias, pen=self.pen_vbia, name="VBIAS")

        cps = self.window.addPlot(title='CPS')
        cps.plot(x=self.indexs, y=self.cpss, pen=self.pen_vbia, name="CPS")

        self.window.nextRow()

        d_start = self.window.addPlot(title='DECHUCK START')
        d_start.plot(x=self.indexs, y=self.dechuck_starts, pen=self.pen_dechuck_start, name="DECHUCK START")

        cps = self.window.addPlot(title='DECHUCK END')
        cps.plot(x=self.indexs, y=self.dechuck_ends, pen=self.pen_dechuck_end, name="DECHUCK END")
        
    def on_accepted(self):
        self.accept()


class ZoomWindowFull(QDialog):

    def __init__(self, Dialog, indexs, voltage1s, current1s, voltage2s, current2s, vbias, cpss, dechuck_starts, dechuck_ends):
        super().__init__()
        self.title = "ZOOM IN DATA"
        self.indexs = indexs 
        self.voltage1s = voltage1s 
        self.current1s = current1s 
        self.voltage2s = voltage2s 
        self.current2s = current2s 
        self.vbias = vbias 
        self.cpss = cpss 
        self.dechuck_starts = dechuck_starts 
        self.dechuck_ends = dechuck_ends 

        self.pen_voltage1 = pyGraph.mkPen(width=1, color=(255, 0, 0))
        self.pen_current1 = pyGraph.mkPen(width=1, color=(0, 0, 255))
        self.pen_voltage2 = pyGraph.mkPen(width=1, color=(255, 191, 0))
        self.pen_current2 = pyGraph.mkPen(width=1, color=(0,255, 0))
        self.pen_vbia = pyGraph.mkPen(width=1, color=(0, 255, 0))
        self.pen_cps = pyGraph.mkPen(width=1, color=(204, 153, 0))
        self.pen_dechuck_start = pyGraph.mkPen(width=1, color=(204, 0, 153))
        self.pen_dechuck_end = pyGraph.mkPen(width=1, color=(102, 0, 204))

        self.setupUI(Dialog)

    def setupUI(self, Dialog):
        self.setGeometry(0, 0, 1200, 800)

        main_layer = QVBoxLayout()
        self.setLayout(main_layer)

        self.window = pyGraph.PlotWidget()
        pyGraph.setConfigOptions(antialias=True)
        main_layer.addWidget(self.window)

        ## create mainAxis
        self.mainAxis = self.window.plotItem
        self.mainAxis.setLabels(left='Voltage')
        self.mainAxis.showAxis('right')

        ## Second ViewBox
        self.axisB = pyGraph.ViewBox()
        self.mainAxis.scene().addItem(self.axisB)
        self.mainAxis.getAxis('right').linkToView(self.axisB)
        self.axisB.setXLink(self.mainAxis)
        self.mainAxis.getAxis('right').setLabel('Current', color='#0000ff')

        ## create Third ViewBox. 
        self.axisC = pyGraph.ViewBox()
        ax3 = pyGraph.AxisItem('right')
        self.mainAxis.layout.addItem(ax3, 2, 3)
        self.mainAxis.scene().addItem(self.axisC)
        ax3.linkToView(self.axisC)
        self.axisC.setXLink(self.mainAxis)
        ax3.setLabel('Vais', color='#00ff00')

        ## Handle view resizing 
        def updateViews():
            ## view has resized; update auxiliary views to match
            # global self.mainAxis, self.axisB, self.axisC
            self.axisB.setGeometry(self.mainAxis.vb.sceneBoundingRect())
            self.axisC.setGeometry(self.mainAxis.vb.sceneBoundingRect())
            
            self.axisB.linkedViewChanged(self.mainAxis.vb, self.axisB.XAxis)
            self.axisC.linkedViewChanged(self.mainAxis.vb, self.axisC.XAxis)

        updateViews()
        self.mainAxis.vb.sigResized.connect(updateViews)

        self.graph_voltage1s =  self.mainAxis.plot(self.indexs, self.voltage1s, pen=self.pen_voltage1, name="Voltage1s")
        self.graph_current1s =  self.axisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current1s, pen=self.pen_current1, name="Current1s"))
        self.graph_voltage2s =  self.mainAxis.plot(self.indexs, self.voltage2s, pen=self.pen_voltage2, name="voltage2s")
        self.graph_current2s =  self.axisB.addItem(pyGraph.PlotCurveItem(self.indexs, self.current2s, pen=self.pen_current2, name="Current2s"))

        self.graph_vbias =  self.axisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.vbias, pen=self.pen_vbia, name="Vbias"))
        self.graph_cpss =  self.mainAxis.plot(self.indexs, self.cpss, pen=self.pen_cps, name="Cps")
        self.graph_dechuck_starts =  self.mainAxis.plot(self.indexs, self.dechuck_starts, pen=self.pen_dechuck_start, name="Dechuck_start")
        self.graph_dechuck_ends =  self.axisC.addItem(pyGraph.PlotCurveItem(self.indexs, self.dechuck_ends, pen=self.pen_dechuck_end, name="Dechuck_end"))

        
    def on_accepted(self):
        self.accept()


def main():
    app = QApplication(sys.argv)
    main = ZoomWindow()
    main.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()